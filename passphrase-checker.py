import itertools, time, getpass, math

characterset = ("aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ1234567890-_.,~`!?@#$%^&*()/\{\}:;<>") # Tuple of characters

def bruteforcer(Password):
    CharLength = counter = 1
    start = time.time() #bruteforce timer
    print("---------------------------------------")
    for CharLength in range(counter, 64): #64 characters is an NIST maximum length
        print(CharLength, "characters long at", round((time.time() - start), 3), "seconds\t")
        passwords = (itertools.product(characterset, repeat = CharLength))
        for i in passwords:
            counter += 1
            i = ''.join(i)
            if i == Password:
                end = time.time()
                counter-=1
                print("---------------------------------------")
                print("TIME TAKEN:", math.floor(end-start), "seconds\nATTEMPTS:", counter, "\nSPEED:", round(counter/(end-start), 2), "attempts per sec!")
                return end

def estimator(Password):
    """
    S=D/t (counter / e-s)
    D=tS (counter=e-s * e-s/2)
    t=D/S (t= counter / counter/e-s)

    1. time taken for 9999 attempts in milliseconds
    2. time taken for 1 attempt in milliseconds
    3. 88^x where x is length of current password to get total permutations needed
    4. total permutations x time taken for one password in milliseconds
    5. Convert to seconds by dividing by 100
    """
    CharLength = counter = 1
    start = time.time() #bruteforce timer
    for CharLength in range(counter, 64): #64 characters is an NIST maximum length
        passwords = (itertools.product(characterset, repeat = CharLength))
        for i in passwords:
            counter += 1
            i = ''.join(i)
            if i == "aaaaa": # dummy value
                end = time.time()
                counter -= 1
                timetaken=(end-start)*1000
                perattempt=timetaken/counter
                estimatedattempts=len(characterset)**len(Password)
                estimatedtime=math.floor(estimatedattempts*perattempt)
                if estimatedtime>=0 and estimatedtime<=(1000)-1:
                    unittime=str(round(estimatedtime, 2))+ " milliseconds"
                    return unittime
                elif estimatedtime>=1000 and estimatedtime<=(1000*60)-1:
                    unittime=str(math.floor(estimatedtime/1000))+" seconds"
                    return unittime
                elif estimatedtime>=1000*60 and estimatedtime<=(1000*60*60)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60)))+" minutes"
                    return unittime
                elif estimatedtime>=1000*60*60 and estimatedtime<=(1000*60*60*24)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60*60)))+" hours"
                    return unittime
                elif estimatedtime>=1000*60*60*24 and estimatedtime<=(1000*60*60*24*30)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60*60*24)))+" days"
                    return unittime
                elif estimatedtime>=1000*60*60*24*30 and estimatedtime<=(1000*60*60*24*30*12)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60*60*24*30)))+" months"
                    return unittime
                elif estimatedtime>=1000*60*60*24*30*12 and estimatedtime<=(1000*60*60*24*30*12*100)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60*60*24*30*12)))+" years"
                    return unittime
                elif estimatedtime>=1000*60*60*24*30*12*100 and estimatedtime<=(1000*60*60*24*30*12*100*1000)-1:
                    unittime=str(math.floor(estimatedtime/(1000*60*60*24*30*12*100)))+" centuries"
                    return unittime
                elif estimatedtime>=1000*60*60*24*30*12*100*1000:
                    unittime=str(math.floor(estimatedtime/(1000*60*60*24*30*12*100*1000)))+" millenia"
                    return unittime

def rater(Password):
    score=0
    rating = "None"
    if len(Password)>=6:
        score+=2
    if len(Password)>=7:
        score+=2
    if len(Password)>=8:
        score+=3
    if len(Password)>=9:
        score+=4
    if any(char.isdigit() for char in Password) is True:
        score+=0.5
    if any(char.isupper() for char in Password) is True:
        score+=0.5
    if any(char.islower() for char in Password) is True:
        score+=0.5
    if any(not char.isalnum() for char in Password) is True:
        score+=0.5
    if score<=2.9:
        rating="horrible"
    if score>=3 and score<=4.9:
        rating="bad"
    if score>=5 and score<=6.9:
        rating="mediocre"
    if score>=7 and score<=8.9:
        rating="good"
    if score>=9:
        rating="strong"
    return rating

Password = getpass.getpass("Enter Passphrase 🗝  ") # silent input 
if Password == "":
    print("Please enter a password!")
    exit()

print("This is a", rater(Password), "password and will take no longer than", estimator(Password), "to bruteforce")
bruteforcer(Password)